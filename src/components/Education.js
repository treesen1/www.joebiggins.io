import React from "react";

const Education = () => (
  <div className="row education">
    <div className="three columns header-col">
      <h1>
        <span>Education</span>
      </h1>
    </div>

    <div className="nine columns main-col">
      <div className="row item">
        <div className="twelve columns">
          <h3>University of Iowa</h3>
          <p className="info">
            Computer Science and Engineering, B.S.E.
            <span>&bull;</span>
            <em className="date">Aug 2015 - May 2020</em>
          </p>

          <p>
			  • CS Courses: Algorithms, Graph Algorithms and Combinatorial Optimization, Machine Learning, Data Visualization and Data Technologies
			  • Engineering Courses: Embedded Systems, Computer Architecture, ComputerNetworks
          </p>
        </div>
      </div>

      <div className="row item">
        <div className="twelve columns">
          <h3>University of Iowa</h3>
          <p className="info">
			  Big Data/Data Mining/Machine Learning, EFA
			  <span>&bull;</span>
            <em className="date">Aug 2018 - May 2020</em>
          </p>
          <p>
    		Courses:
          </p>
        </div>
      </div>
    </div>
  </div>
);

export default Education;
