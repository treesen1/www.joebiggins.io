import React from "react";

const Work = () => (
  <div className="row work">
    <div className="three columns header-col">
      <h1>
        <span>Work</span>
      </h1>
    </div>

    <div className="nine columns main-col">
      <div className="row item">
        <div className="twelve columns">
          <h3>Mostardi-Platt</h3>
          <p className="info">
            Engineer II, Intern
            <span>&bull;</span>
            <em className="date">May 2019 - August 2019</em>
          </p>

          <p>
			  • Developed software to mitigate human input, and facilitate the execution of Method 5 testing;
			  effectively automating particulate testing.
			  • Integrated negative feedback control loop that implemented the automation software with small, motor-driven valves to
			  further automate the totality of isokinetic air emissions testing.
			  • Implemented several API microservices in Node.jsKoa and in the serverless AWS Lambdafunctions.
		  </p>
        </div>
      </div>

      <div className="row item">
        <div className="twelve columns">
          <h3>T.H. Brokerage</h3>
          <p className="info">
			  Options Trading Automation Intern
			  <span>&bull;</span>
            <em className="date">May 2019 - August 2020</em>
          </p>

          <p>
			  • Built and deployed overall service in frastructure utilizing Docker container, CircleCI, and several AWS stack
			  	(Including EC2, ECS, Route53, S3, CloudFront, RDS, ElastiCache, IAM), focusing on high-availability, fault tolerance, and auto-scaling.
			  • Developed an easy-to-use Payment module which connects to major PG (PaymentGateway) companiesin Hong Kong.
			  • Performed quantitative market analysis, andimplemented ML algorithms to augment data collection effectiveness
          </p>
        </div>
      </div>
    </div>
  </div>
);

export default Work;
