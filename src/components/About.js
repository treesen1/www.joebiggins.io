import React from "react";
import { FaCloudDownload } from "react-icons/lib/fa";

import profilePic from "../assets/images/0.jpg";

const About = () => (
  <section id="about">
    <div className="row">
      <div className="three columns">
        <img className="profile-pic" src={profilePic} alt="" />
      </div>
      <div className="nine columns main-col">
        <h2>About Me</h2>
        <p>
			Seeking Full-Time Employment Opportunities. Willing to Relocate.
        </p>
        <div className="row">
          <div className="columns contact-details">
            <h2>Contact Details</h2>
            <p className="address">
              <span>Joe Biggins</span>
              <br />
              <span>
                306 S. Arlington Ave.
                <br /> Elmhurst, IL 60126 US
              </span>
              <br />
              <span>+1 (630) 842-0626</span>
              <br />
              <span>joseph-biggins@uiowa.edu</span>
				<br />
				<span>jjbiggins@joebiggins.io</span>
            </p>
          </div>
          <div className="columns download">
            <p>
              <a href="https://cvws.icloud-content.com/B/AQKDhmEUncifRraJ4ajN1DetjuTiAXkP_8cvEffCjIAUB9OTKQk4qann/cv+copy.pdf?o=AkeqhfhNiLbtanlhlQtgNp-rS1vFThoMXoyPnFMGD7F8&v=1&x=3&a=CAogr-52urWDcaziavUX0E0aG-I7mh78BjUBJNhGdulZ1yUSbRDGmYj2qS4Y5pC_9qkuIgEAUgStjuTiWgQ4qannaiZUUFavk2DEf06Luar3lDSqOFIRRTjWxEIa5cc1-0SIjiUJwG5nsXImiM_KxGV756R7hF6GxfWTUoJ_1Kh4_--ztVnayveSOmj9dMnM8tE&e=1591802316&fl=&r=d96f764e-0318-45ba-a361-5eb6d0f4f24a-1&k=J6DFCyEh3varrocuq2JQJw&ckc=com.apple.clouddocs&ckz=com.apple.CloudDocs&p=60&s=l0f1TemQh8xUIlCmyuuqdtNvYzY&cd=i" className="button">
                <FaCloudDownload /> Download Resume
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
);
// TODO:https://cvws.icloud-content.com/B/AQKDhmEUncifRraJ4ajN1DetjuTiAXkP_8cvEffCjIAUB9OTKQk4qann/cv+copy.pdf?o=AkeqhfhNiLbtanlhlQtgNp-rS1vFThoMXoyPnFMGD7F8&v=1&x=3&a=CAogr-52urWDcaziavUX0E0aG-I7mh78BjUBJNhGdulZ1yUSbRDGmYj2qS4Y5pC_9qkuIgEAUgStjuTiWgQ4qannaiZUUFavk2DEf06Luar3lDSqOFIRRTjWxEIa5cc1-0SIjiUJwG5nsXImiM_KxGV756R7hF6GxfWTUoJ_1Kh4_--ztVnayveSOmj9dMnM8tE&e=1591802316&fl=&r=d96f764e-0318-45ba-a361-5eb6d0f4f24a-1&k=J6DFCyEh3varrocuq2JQJw&ckc=com.apple.clouddocs&ckz=com.apple.CloudDocs&p=60&s=l0f1TemQh8xUIlCmyuuqdtNvYzY&cd=i
export default About;
