import React from "react";

const Skills = () => (
  <div className="row skill">
    <div className="three columns header-col">
      <h1>
        <span>Skills</span>
      </h1>
    </div>

    <div className="nine columns main-col">
      <p>
			The Number of Years Experience I have in a few prominent Programming Languages
      </p>

      <div className="bars">
        <ul className="skills">
          <li>
			  <span className="bar-expand photoshop" >5 years</span>
            <em>Python</em>
          </li>
          <li>
            <span className="bar-expand illustrator"  >5 years</span>
            <em>Java</em>
          </li>
          <li>
            <span className="bar-expand wordpress"  >5 years</span>
            <em>C/C++</em>
          </li>
          <li>
            <span className="bar-expand css"  >2 years</span>
            <em>CSS/HTML</em>
          </li>
          <li>
            <span className="bar-expand html5"  >2 years</span>
            <em>JavaScript</em>
          </li>
          <li>
            <span className="bar-expand shell" >3 years</span>
            <em>Unix Shell</em>
          </li>
        </ul>
      </div>
    </div>
  </div>
);

export default Skills;
